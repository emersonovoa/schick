<?php

if (session_id() == "") session_start();
ob_start();

if(isset($_GET['u_id']) && $_SESSION["user_id"] != ""):	

	$user_id = $_GET['u_id'];
	$suma    = (int)$_GET['r_s'];

	$text = "";
	$photo_perfil = "";
	$profile = 0;
    $fontstyle = "";

	if ($suma > 0 && $suma <= 5) {

		$text = "Rasurado al ras: este tipo de rasurado se recomienda especialmente para hombres que buscan un aspecto más pulcro, limpio y formal. De igual manera se puede utilizar cuando el crecimiento no es abundante y con espacios. Es recomendable utilizar siempre tu maquina Hydro en la posición vertical con el fin de lograr un rasurado al ras perfecto y un delineado de patilla preciso.";
		$photo_perfil = "assets/img/Comp-1.gif";
		$profile = 1;
        $fontsize = "font-size: 15px";

	} elseif ($suma >= 6 && $suma <= 10) {

		$text = "Natural: La barba de 7 días es recomendable para hombres con un crecimiento completo de la barba, pero que buscan verse con un look más fresco y despreocupado, Hydro siempre recomienda hacer un delineado de los bordes con el fin de que luzca una barba casual pero cuidada y elegante. ";
		$photo_perfil = "assets/img/Comp-2.gif";
		$profile = 2;
        $fontsize = "font-size: 16px;line-height: 20px;";

	} elseif ($suma >= 11 && $suma <= 15){

		$text = "Ejecutivo: Hydro recomienda este tipo para hombres que buscan una barba abundante pero sin dejar la elegancia y formalidad de lado, el crecimiento abundante pero con bordes muy marcados y limpios.";
		$photo_perfil = "assets/img/Comp-3.gif";
		$profile = 3;
        $fontsize = "font-size: 18px;line-height: 24px;";

	} elseif ($suma >= 16){

		$text = "Full Beard: para hombres que buscan madurez y extravagancia en su vida, la barba completo y larga sin dejar de perfilar con el fin de no desalinear el look.";
		$photo_perfil = "assets/img/Comp-4.gif";
		$profile = 4;
        $fontsize = "font-size: 18px;line-height: 24px;";

	}
	
	require_once('include/Config_GET.php');

	$sql = "SELECT * FROM users WHERE ID_User = ".$user_id;	

	$result = $conn->query($sql);

	$numRows = $result->num_rows;

	if ($numRows > 0 ):

		while ($arrayRs = $result->fetch_assoc()){
			$user_name  = utf8_encode($arrayRs['Name']);
			$user_email = $arrayRs['Email'];
		}

	endif;

	$sql_i = "INSERT INTO user_profile(ID_User, ID_Profile, Points, Date_Mod)
	          VALUES (".$user_id.",".$profile.",".$suma.",NOW());";

	$result_i = $conn->query($sql_i);

?>


<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="">
	<link rel="icon" type="image/png" href="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Schick Hydro Sensitive</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/material-kit.css" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet"/>
    <style type="text/css">body{ overflow-x: hidden; overflow-y: hidden;} </style>
</head>

<body id="bg-sh">
<div id="pleca"></div>
   <div class="section section-full-screen" >
			<div class="container" >

				<div class="row">

	                <div class="col-md-8 col-md-offset-2 text-center">
	                    <img class="n-logo" src="./assets/img/Logo-shick.png" alt="">
	                    
	                </div>

					<div class="col-md-8 col-md-offset-2" id="hydro-res">
							<div  class="row" >
									<div  >
				                       <img src="<?=$photo_perfil;?>" alt="Thumbnail Image" class=" img-responsive img-final img-rounded"  >
				                    </div>								

						    	<div class="confirm-text confirm-back" >
							    	<h3 ><?=$user_name;?></h3>
							    	<div id="pl"> 
							    	  <p  style="<?=$fontsize?>"><?=$text?></p>							    	  
						        </div>  
					        </div>
					</div>
				</div>
			</div>
						
			<div id="hydro">
		         <div >
			         <img  src="./assets/img/hydro.png"  >
				</div>
		    </div>
    </div>

</body>


	<!--   Core JS Files   -->
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/material.min.js"></script>

	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="assets/js/nouislider.min.js" type="text/javascript"></script>

	<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
	<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

	<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
	<script src="assets/js/material-kit.js" type="text/javascript"></script>

</html>

<?php

/* Mail sendind */
$destinatario = $user_email;
$asunto = "Este es tu resultado";
require 'utils/class.phpmailer.php';
$mail = new PHPMailer();

//Luego tenemos que iniciar la validaci&oacute;n por SMTP:
$mail->SMTPKeepAlive = true;  
$mail->Mailer = "smtp"; 
$mail->IsSMTP();
$mail->SMTPSecure = "ssl";  // set mailer to use SMTP

$mail->Host = "s200443.gridserver.com";  // specify main and backup server
$mail->Port= "465";
$mail->SMTPAuth = true;     // turn on SMTP authentication
$mail->Username = "contact@renegadeisthejeep.com";  // SMTP username
$mail->Password = "m6yEh4_D_d"; // SMTP password

//Con estas pocas l&iacute;neas iniciamos una conexi&oacute;n con el SMTP. Lo que ahora deber&iacute;amos hacer, es configurar el mensaje a enviar, el //From, etc.
$mail->From = "contacto@schick.com"; // Desde donde enviamos (Para mostrar)
$mail->FromName = "SCHICK";
$mail->SingleTo = true;                
$mail->AddAddress($destinatario); // Esta es la direcci&oacute;n a donde enviamos            
$mail->IsHTML(true); // El correo se env&iacute;a como HTML
$mail->Subject = $asunto; // Este es el titulo del email.

$body = '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <style type="text/css">
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        
        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
                
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            Margin: 0 auto !important;
        }
        table table table {
            table-layout: auto; 
        }
        
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        
        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }
      
    </style>
    
    <!-- Progressive Enhancements -->
    <style>
        
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

        .row {
            margin-left: -15px;
            margin-right: -15px;
        }
        .text-center {
            text-align: center;
            position: relative;
            margin-bottom: 200px;
        }
        img {
            vertical-align: middle;
        }
        .ficha {
            position: relative;
            padding-left: 15px;
            padding-right: 15px;
            text-align: center;
            margin-top: -90px;
        }
        .confirm-back {
            margin-left: 60px;
            padding-left: 30px;
            padding-top: 10px;
            background-color: #76E500;
            width: 300px;
            height: 140px;
            margin-left: 80px;
        }
        #hydro{            
            width: 600px;
            max-width: 600px;
        }

        #hydro img {
            float: right;
            width: 200px;            
            margin-right: 90px;
            margin-top: 30px;
        }
        .footer-basic-left {
            text-align: left;
            font: normal 12px sans-serif;
            color: #ffffff;            
            padding: 15px;
            bottom: 0;
            padding-left: 10%;
            float: left;
            padding-top: 280px;
        }
        .img-final{
            /*width:220px;*/
            width: 60%;
            float: right;
             margin-right: 50px; 
             margin-top: -55px; 
             position: relative;
             background: #76E500;
             box-shadow: 0 2px 53px 0 #115994;
        }
        .img-rounded {
              border-radius: 350px;
              transition: opacity 0.5s ease 0s;
              max-width: 100%;
              padding: 7px;
              margin-top: 0;
        }
        .footer-company-name a{
            text-decoration: none;
            color: #76E500;
            
        }
        .footer-company-name{
            text-align: left;
            font: normal 12px sans-serif;
            padding-left: 30px;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
                margin: auto !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid,
            .fluid-centered {
                max-width: 100% !important;
                height: auto !important;
                Margin-left: auto !important;
                Margin-right: auto !important;
            }
            /* And center justify these ones. */
            .fluid-centered {
                Margin-left: auto !important;
                Margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }
        
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                Margin-left: auto !important;
                Margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }
                
        }

    </style>

</head>
<body bgcolor="#FFF" width="100%" style="Margin: 0;">
    <center style="width: 100%; background: #FFF;">        
        <!-- Email Body : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="center" background="http://lowemx.com/schick/assets/img/End2.png" width="600" style="margin: auto;" class="email-container">
            
            <!-- Hero Image, Flush : BEGIN -->
            <tbody>

            <tr style="text-align: center;">
                <td>
                    <a href="http://www.schicklatinoamerica.com/">
                        <img class="n-logo" src="http://lowemx.com/schick/assets/img/Logo-shick.png" alt="">
                    </a>
                </td>
            </tr>            

            <!-- Thumbnail Left, Text Right : BEGIN -->
            <tr>
                <td dir="ltr" align="center" valign="top" width="100%" style="padding: 10px;">
                    <table style="/* background-color: #76E500; */" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody><tr>
                            <!-- Column : BEGIN -->
                            <td width="100%" class="stack-column-center">
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody><tr>
                                        <td dir="ltr" valign="top" style="font-family: sans-serif; font-size: 12px; mso-height-rule: exactly; line-height: 20px; color: #555555; text-align: left;" class="footer-company-name">
                                                <h3 style="color: #76E500;font-size: 26px;line-height: 27px;">'.$user_name.'</h3>                                                
                                                <p style="color: #76E500;font-size: 15px;">'.$text.'</p>         
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                            <!-- Column : END -->
                            <!-- Column : BEGIN -->
                            <td width="100%" class="stack-column-center">
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody><tr>
                                        <td dir="ltr" valign="top" style="padding: 0 10px;">
                                            <img src="http://lowemx.com/schick/'.$photo_perfil.'" alt="Thumbnail Image" class=" img-responsive img-final img-rounded">
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                            <!-- Column : END -->
                        </tr>
                    </tbody></table>
                </td>
            </tr>
            <!-- Thumbnail Left, Text Right : END -->            

            <tr style="text-align: center;">
                <td>
                    <a href="http://www.schicklatinoamerica.com/">
                        <img style="width: 200px;" src="http://lowemx.com/schick/assets/img/hydro.png" alt="">
                    </a>
                </td>
            </tr>

            <tr style="text-align: center;">
                <td dir="ltr" valign="top" style="font-family: sans-serif; font-size: 12px; mso-height-rule: exactly; line-height: 20px; color: #555555; text-align: center;padding-left: 0px !important;" class="footer-company-name">
                        <p>
                            <a href="http://lowemx.com/schick/terminosycondiciones.html">
                            Términos y condiciones
                            </a>
                        </p>                                            
                </td>
            </tr>

        </tbody></table>
        <!-- Email Body : END -->        

    </center>



</body>
</html>';


$mail->Body = $body; // Mensaje a enviar
//echo $body;
$mail->Send(); // Env&iacute;a el correo.
$mail->ClearAddresses();
/* End mail sendind */

session_destroy();

else:
	//session_destroy();
  	header("Location: index.php");
endif;

?>