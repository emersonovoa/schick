<?php

if (session_id() == "") session_start();
ob_start();

if(isset($_SESSION) && $_SESSION["user_id"] != ""):

	$session_user_id    = $_SESSION["user_id"];
	$session_user_email = $_SESSION["email"];

	require_once('include/Config_GET.php');

	$sql = "SELECT * FROM users WHERE ID_User = ".$session_user_id;

	$result = $conn->query($sql);

	$numRows = $result->num_rows;

	if ($numRows > 0 ):

		while ($arrayRs = $result->fetch_assoc()){			
			$user_email = $arrayRs['Email'];
		}

	endif;

	if($user_email == $session_user_email):

?>

		<!doctype html>
		<html lang="en">
		<head>
			<meta charset="utf-8" />
			<link rel="apple-touch-icon" sizes="76x76" href="">
			<link rel="icon" type="image/png" href="">
			<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

			<title>Schick Hydro Sensitive</title>

			<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />

			<!--     Fonts and icons     -->
			<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
		    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

			<!-- CSS Files -->
		    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
		    <link href="assets/css/material-kit.css" rel="stylesheet"/>
		    <link href="assets/css/demo.css" rel="stylesheet"/>
		</head>

		<body class="section-full-screen" id="bg-sh">

			<div id="pleca"></div>
                
                <div class="section">

						<input type="hidden" id="user_id" name="user_id" value="<?=$session_user_id?>">			
						<!-- 1st question -->
					<div id="div_qs_1" class="col-md-8 col-md-offset-2 carduno" >

						<div class="card-ques" >

							<div style="display: table-row;">

							    <div style="display: table-cell;width: 8%;">
								    <div class="triangulo" id="space-triangulo" ></div>
								</div>

								<div style="display: table-cell;">

									<div class="check" style="display: table; ">
										<div style="display: table-row; ">
											<div style="display: table-cell;">
												<div class="pregunta">¿Cuál es tu rango de edad?</div>
											</div>
										</div>
									</div>

									<div style="display: table;width: 100%;">

						        		<div style="display: table-row; ">

											<div style="display: table-cell;">
												<div class="checkbox">
													<label>
														<input id="1-1" type="checkbox" name="optionsCheckboxes1" value="0" class="group1">
														25-30
													</label>
												</div>
											</div>
											
											<div style="display: table-cell;">
												<div class="checkbox">
													<label >
														<input id="1-2" type="checkbox" name="optionsCheckboxes1" value="1" class="group1">
														30-35
													</label>
												</div>
											</div>	
													
										    <div style="display: table-cell;">
										    	<div class="checkbox">
													<label >
														<input id="1-3" type="checkbox" name="optionsCheckboxes1" value="2" class="group1">
														35-40
													</label>
												</div>
											</div>

											<div style="display: table-cell;">
										    	<div class="checkbox">
													<label >
														<input id="1-4" type="checkbox" name="optionsCheckboxes1" value="3" class="group1">
														40+
													</label>
												</div>
											</div>

										</div>

									</div>

					            </div>

						 	</div>

						</div>

					</div><!-- End 1st question -->

					<!-- 2nd question -->
					<div id="div_qs_2" class="col-md-8 col-md-offset-2 carddos">

						<div class="card-ques" style="display: table; vertical-align: middle; " >

							<div style="display: table-row;">

							    <div style="display: table-cell;width: 8%;">
								    <div class="triangulo" id="space-triangulo" ></div>
								</div>

								<div style="display: table-cell;">

									<div class="check" style="display: table; ">
										<div style="display: table-row; ">
											<div style="display: table-cell;">
												<div class="pregunta">¿Qué forma de cara tienes?</div>
											</div>
										</div>
									</div>

									<div style="display: table;width: 100%;">

						        		<div style="display: table-row; ">

											<div style="display: table-cell;">
												<div class="checkbox">
													<label >
														<input id="2-1" type="checkbox" name="optionsCheckboxes2" value="0">
														Redonda
													</label>
												</div>
											</div>
											
											<div style="display: table-cell;">
												<div class="checkbox">
													<label >
														<input id="2-2" type="checkbox" name="optionsCheckboxes2" value="1">
														Ovalada 
													</label>
												</div>
											</div>	
													
										    <div style="display: table-cell;">
										    	<div class="checkbox">
													<label >
														<input id="2-3" type="checkbox" name="optionsCheckboxes2" value="2">
														Cuadrada
													</label>
												</div>
											</div>

											<div style="display: table-cell;">
										    	<div class="checkbox">
													<label >
														<input id="2-4" type="checkbox" name="optionsCheckboxes2" value="3">
														Alargada
													</label>
												</div>
											</div>

										</div>

									</div>

					            </div>

						 	</div>

						</div>

					</div><!-- End 2nd question -->

					<!-- 3rd question -->
					<div id="div_qs_3" class="col-md-8 col-md-offset-2 carddos">

						<div class="card-ques" style="display: table; vertical-align: middle; " >

							<div style="display: table-row;">

							    <div style="display: table-cell;width: 8%;">
								    <div class="triangulo" id="space-triangulo" ></div>
								</div>

								<div style="display: table-cell;">

									<div class="check" style="display: table; ">
										<div style="display: table-row;">
											<div style="display: table-cell;">
												<div class="pregunta">¿Qué tipo de crecimiento tiene tu barba?</div>
											</div>
										</div>
									</div>

									<div style="display: table;width: 100%;">

						        		<div style="display: table-row; ">

											<div style="display: table-cell;">
												<div class="checkbox">
													<label>
														<input id="3-1" type="checkbox" name="optionsCheckboxes3" value="3">
														Cerrada
													</label>
												</div>
											</div>
											
											<div style="display: table-cell;">
												<div class="checkbox">
													<label >
														<input id="3-2" type="checkbox" name="optionsCheckboxes3" value="2">
														Cerrada con crecimiento mediano 
													</label>
												</div>
											</div>	
													
										    <div style="display: table-cell;">
										    	<div class="checkbox">
													<label >
														<input id="3-3" type="checkbox" name="optionsCheckboxes3" value="1">
														Poca y con espacios
													</label>
												</div>
											</div>

											<div style="display: table-cell;">
										    	<div class="checkbox">
													<label >
														<input id="3-4" type="checkbox" name="optionsCheckboxes3" value="0">
														Casi nada de crecimiento
													</label>
												</div>
											</div>

										</div>

									</div>

					            </div>

						 	</div>

						</div>

					</div><!-- End 3rd question -->

					<!-- 4th question -->
					<div id="div_qs_4" class="col-md-8 col-md-offset-2 carddos">

						<div class="card-ques" style="display: table; vertical-align: middle; " >

							<div style="display: table-row;">

							    <div style="display: table-cell;width: 8%;">
								    <div class="triangulo" id="space-triangulo" ></div>
								</div>

								<div style="display: table-cell;">

									<div class="check" style="display: table; ">
										<div style="display: table-row;">
											<div style="display: table-cell;">
												<div class="pregunta">¿Qué tipo de trabajo tienes?</div>
											</div>
										</div>
									</div>

									<div style="display: table;width: 100%;">

						        		<div style="display: table-row; ">

											<div style="display: table-cell;">
												<div class="checkbox">
													<label >
														<input id="4-1" type="checkbox" name="optionsCheckboxes4" value="4">
														En oficina, formal
													</label>
												</div>
											</div>

											<div style="display: table-cell;">
												<div class="checkbox">
													<label >
														<input id="4-2" type="checkbox" name="optionsCheckboxes4" value="1">
														Freelance
													</label>
												</div>
											</div>
											
											<div style="display: table-cell;">
												<div class="checkbox">
													<label >
														<input id="4-3" type="checkbox" name="optionsCheckboxes4" value="3">
														En oficina, informal 
													</label>
												</div>
											</div>	
													
										    <div style="display: table-cell;">
										    	<div class="checkbox">
													<label >
														<input id="4-4" type="checkbox" name="optionsCheckboxes4" value="2">
														De Campo
													</label>
												</div>
											</div>

											<div style="display: table-cell;">
										    	<div class="checkbox">
													<label >
														<input id="4-5" type="checkbox" name="optionsCheckboxes4" value="0">
														No trabajo
													</label>
												</div>
											</div>

										</div>

									</div>

					            </div>

						 	</div>

						</div>

					</div><!-- End 4th question -->

					<!-- 5th question -->
					<div id="div_qs_5"  class="col-md-8 col-md-offset-2 carddos">

						<div class="card-ques" style="display: table; vertical-align: middle; " >

							<div style="display: table-row;">

							    <div style="display: table-cell;width: 8%;">
								    <div class="triangulo" id="space-triangulo" ></div>
								</div>

								<div style="display: table-cell;">

									<div class="check" style="display: table; ">
										<div style="display: table-row;">
											<div style="display: table-cell;">
												<div class="pregunta">¿Qué tipo de corte de pelo tienes?</div>
											</div>
										</div>
									</div>

									<div style="display: table;width: 100%;">

						        		<div style="display: table-row; ">

											<div style="display: table-cell;">
												<div class="checkbox">
													<label >
														<input id="5-1" type="checkbox" name="optionsCheckboxes5" value="1">
														Largo y con volumen
													</label>
												</div>
											</div>
											
											<div style="display: table-cell;">
												<div class="checkbox">
													<label >
														<input id="5-2" type="checkbox" name="optionsCheckboxes5" value="2">
														Corto y con volumen 
													</label>
												</div>
											</div>	
													
										    <div style="display: table-cell;">
										    	<div class="checkbox">
													<label >
														<input id="5-3" type="checkbox" name="optionsCheckboxes5" value="3">
														Corto y con poco volumen
													</label>
												</div>
											</div>

											<div style="display: table-cell;">
										    	<div class="checkbox">
													<label >
														<input id="5-4" type="checkbox" name="optionsCheckboxes5" value="4">
														Muy corto
													</label>
												</div>
											</div>

										</div>

									</div>

					            </div>

						 	</div>

						</div>

					</div><!-- End 5th question -->

					<!-- 6th question -->
					<div id="div_qs_6" class="col-md-8 col-md-offset-2 carddos" >

						<div class="card-ques" style="display: table; vertical-align: middle; " >

							<div style="display: table-row;">

							    <div style="display: table-cell;width: 8%;">
								    <div class="triangulo" id="space-triangulo" ></div>
								</div>

								<div style="display: table-cell;">

									<div class="check" style="display: table; ">
										<div style="display: table-row;">
											<div style="display: table-cell;">
												<div class="pregunta">¿Cuál es tu personalidad?</div>
											</div>
										</div>
									</div>

									<div style="display: table;width: 100%;">

						        		<div style="display: table-row; ">

											<div style="display: table-cell;">
												<div class="checkbox">
													<label >
														<input id="6-1" type="checkbox" name="optionsCheckboxes6" value="3">
														Extrovertido
													</label>
												</div>
											</div>
											
											<div style="display: table-cell;">
												<div class="checkbox">
													<label >
														<input id="6-2" type="checkbox" name="optionsCheckboxes6" value="1">
														Original
													</label>
												</div>
											</div>	
													
										    <div style="display: table-cell;">
										    	<div class="checkbox">
													<label >
														<input id="6-3" type="checkbox" name="optionsCheckboxes6" value="4">
														Formal
													</label>
												</div>
											</div>

											<div style="display: table-cell;">
										    	<div class="checkbox">
													<label >
														<input id="6-4" type="checkbox" name="optionsCheckboxes6" value="2">
														Relajado
													</label>
												</div>
											</div>

										</div>

									</div>

					            </div>

						 	</div>

						</div>

					</div><!-- End 6th question -->

					<div class="col-md-8 col-md-offset-2 text-center">      
							
							<button  onclick="doEvaluation();" id="div_send_button" class="btn btn-success btn-send btn-send-quest" data-toggle="modal" data-target="#myModal" >
							  Enviar
							</button>
							<!-- Modal Core -->
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
							    <div class="modal-content">
							      <div class="modal-header">
							      	<h5 class="modal-title-confirmacion">Confirmar</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							        
							      </div>
							      <div class="modal-body">
							        
							      </div>
							      <div class="modal-footer ">
							      	<div class="left-side">
							      		<button type="button" class="btn btn-info " data-dismiss="modal">Editar</button>
							      	</div>
							      	<div class="right-side">
							      		<div id="div_submit_questions" onclick="doQuestionsSubmit();" class="btn-space text-center" style="pointer-events: none; opacity: .3;">
									    	<a id="a_submit_question" href="javascript:void(0)" class="btn btn-success "> Ver resultado
									    	</a>
										</div>
							      	</div>					        
							        
							      </div>
							    </div>
							  </div>
							</div>
				  </div>
			</div>
			

		</body>

		<!--   Core JS Files   -->
		<script src="assets/js/jquery.min.js" type="text/javascript"></script>
		<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="assets/js/material.min.js"></script>

		<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
		<script src="assets/js/nouislider.min.js" type="text/javascript"></script>

		<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
		<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

		<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
		<script src="assets/js/material-kit.js" type="text/javascript"></script>
		<script src="assets/js/functions.js" type="text/javascript"></script>

		</html>

<?php

	else:
		header("Location: index.php");
	endif;		

else:
  	header("Location: index.php");
endif;

?>