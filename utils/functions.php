<?php
/**
 * @author Emerson Novoa 
 * @from OVEJANEGRALOWE
*/
 
function getYearFromVIN($vin) {
  
  $years = array();
  $letteryear = substr($vin,9,1);

  switch ($letteryear) {
    case 'A':
      $year_1 = "1980";
      $year_2 = "2010";
      break;
    case 'B':
      $year_1 = "1981";
      $year_2 = "2011";
      break;
    case 'C':
      $year_1 = "1982";
      $year_2 = "2012";
      break;
    case 'D':
      $year_1 = "1983";
      $year_2 = "2013";
      break;
    case 'E':
      $year_1 = "1984";
      $year_2 = "2014";
      break;
    case 'F':
      $year_1 = "1985";
      $year_2 = "2015";
      break;
    case 'G':
      $year_1 = "1986";
      $year_2 = "2016";
      break;
    case 'H':
      $year_1 = "1987";
      $year_1 = "2017";
      break;
    case 'J':
      $year_1 = "1988";
      $year_2 = "2018";
      break;    
    case 'K':
      $year_1 = "1989";
      $year_2 = "2019";
      break;
    case 'L':
      $year_1 = "1990";
      $year_2 = "2020";
      break;
    case 'M':
      $year_1 = "1991";
      $year_2 = "2021";
      break;
    case 'N':
      $year_1 = "1992";
      $year_2 = "2022";
      break;
    case 'P':
      $year_1 = "1993";
      $year_2 = "2023";
      break;
    case 'R':
      $year_1 = "1994";
      $year_2 = "2024";
      break;
    case 'S':
      $year_1 = "1995";
      $year_2 = "2025";
      break;
    case 'T':
      $year_1 = "1996";
      $year_2 = "2026";
      break;
    case 'V':
      $year_1 = "1997";
      $year_1 = "2027";
      break;
    case 'W':
      $year_1 = "1998";
      $year_2 = "2028";
      break;
    case 'X':
      $year_1 = "1999";
      $year_2 = "2029";
      break;
    case 'Y':
      $year_1 = "2000";
      $year_2 = "2030";
      break;
    case '1':
      $year_1 = "2001";
      $year_2 = "2031";
      break;
    case '2':
      $year_1 = "2002";
      $year_2 = "2032";
      break;
    case '3':
      $year_1 = "2003";
      $year_2 = "2033";
      break;
    case '4':
      $year_1 = "2004";
      $year_2 = "2034";
      break;
    case '5':
      $year_1 = "2005";
      $year_2 = "2035";
      break;
    case '6':
      $year_1 = "2006";
      $year_2 = "2036";
      break;
    case '7':
      $year_1 = "2007";
      $year_2 = "2037";
      break;
    case '8':
      $year_1 = "2008";
      $year_2 = "2038";
      break;
    case '9':
      $year_1 = "2009";
      $year_2 = "2039";
      break;
  }

  $years[] = array('year_1' => $year_1, 'year_2' => $year_2);

  return $years;

}

function clean($string) {
   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function getChances($quantity) { 

  $clean_quantity = clean($quantity);
  $out_2final_positions = substr($clean_quantity, 0, -2);
  $required = 1999;
  $bill = (int)$out_2final_positions;

  if ($bill >= $required) {

    $result = round($bill/$required);    

  } else {

    $return = 0;

  }  

  return $result;

}

?>