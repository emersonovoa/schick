<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="">
	<link rel="icon" type="image/png" href="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Schick Hydro Sensitive</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
	<style type="text/css">body{ overflow-x: hidden; overflow-y: hidden;} </style>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/material-kit.css" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet"/>
</head>

<body id="bg-sh">
<div id="pleca"></div>


<div class="section section-full-screen  " >
			<div class="container" id="space">

				<div class="row">

	                <div class="text-center">
	                    <img class="n-logo" src="./assets/img/Logo-shick.png" alt="">
	                    
	                </div>
                
            
					<div class="login-desk col-md-8 col-md-offset-2 ">
						<div  >
							<form class="form" method="" action="">								
								<div>
									<div class="content">

									<div class="input-group card label-floating" >
										<input id="name" type="text" class="form-control" placeholder="   Nombre..." />
										<div class="div-form-error">
											<span id="name-error" class="form-alert form-no-display">
			                                    No es un nombre válido.
			                                </span>
		                                </div>
									</div>

									<div class="input-group card">
										<input id="email" type="text" class="form-control" placeholder="   Email..." />
										<div class="div-form-error">
											<span id="email-error" class="form-alert form-no-display">
			                                    No es un email válido.
			                                </span>			                            
			                                <span id="email-error-exist" class="form-alert form-no-display">
			                                    Existe previamente un registro con este email.
			                                </span>
			                            </div>
									</div>

									<div class="input-group card">
										<input id="telephone" type="text" class="form-control"   placeholder="   Tel&eacute;fono..." onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10"/>
									</div>

									<div class="checkbox text-center" >
										<label >
											<input id="checkbox_tc" type="checkbox" name="optionsCheckboxes" >
											
										<span style="color: #fff;"> Acepto</span>

												<button type="button" class="btn-simple" data-toggle="modal" data-target=".bd-example-modal-sm">T&eacute;rminos y condiciones</button>

												<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
												  <div class="modal-dialog modal-sm">
												    <div class="modal-content">
												    	 <div class="modal-header">
												        <h5 class="modal-title" id="titulo-terminos">T&eacute;rminos y condiciones</h5>
												        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
												          <span aria-hidden="true">&times;</span>
												        </button>
												      </div>
												      <div class="modal-body" id="terminos">
												        <p>El presente Aviso de Privacidad tiene por objeto la protección de los datos personales de los participantes a este test, a efecto de garantizar su privacidad. Dichos datos serán utilizados únicamente con el fin de enviar información, resultado de este test de manera informativa, así como envío de información referente a la marca.</p>
												      </div>
												     
												    </div>
												  </div>
												</div>
										</label>
									</div> 

									<div id="div_submit_button" class="text-center" style="pointer-events: none; opacity: .3;">
										<a href="javascript:void(0)" class="btn  btn-success " onclick="submitForm();">	Enviar
										</a>
									</div>
								</div>
								
							</form>
						</div>



					</div>
				</div>
			</div>
		</div>





</body>

	<!--   Core JS Files   -->
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/material.min.js"></script>

	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="assets/js/nouislider.min.js" type="text/javascript"></script>

	<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
	<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

	<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
	<script src="assets/js/material-kit.js" type="text/javascript"></script>
	<script src="assets/js/functions.js" type="text/javascript"></script>

</html>
