/* Valitation functions */

function isValidEmailAddress( emailAddress ) {
    var result = "";
    var lastChar = emailAddress.substr( emailAddress.length - 1 );
    if ( lastChar == " " ) {
        console.log( "Email con espacio al final. Por aquello del texto predictivo en los moviles. Lo removemos." );
        var result = emailAddress.replace( / /g, "" );
        console.log( result );
    } else {
        result = emailAddress;
        console.log( "Email con espacio al final. Por aquello del texto predictivo en los mobiles" );
    }

    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test( result );

}

function submitForm() {

    var name_user       = $("#name").val();
    var email_user      = $("#email").val();
    var telephone_user = $("#telephone").val();
        
    var formData = new FormData();    

    var flag_validate = false;

    if (name_user == "" || name_user.length < 3 ) {

        flag_validate = false;

        $( '#name-error' ).show();
        $( '#email-error' ).hide();
        $( '#email-error-exist' ).hide();

    } else if (email_user == "" || !isValidEmailAddress( email_user )) {

        flag_validate = false;

        $( '#name-error' ).hide();
        $( '#email-error' ).show();
        $( '#email-error-exist' ).hide();

    } else {

        flag_validate = true;

    }

    if ( flag_validate ) {

            var httpm = new XMLHttpRequest();
            var urlm = "include/existEmail.php";
            var paramsm = "email_user=" + email_user;
            httpm.open( "POST", urlm, true );
            httpm.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
            httpm.onreadystatechange = function () {
                if ( httpm.readyState == 4 && httpm.status == 200 ) {
                    var valEmail = httpm.responseText;
                    if ( valEmail == "True" ) {

                        $( '#name-error' ).hide();
                        $( '#email-error' ).hide();
                        $( '#email-error-exist' ).show();                        

                    } else {

                        // Add the file to the request.
                        formData.append('name_user', name_user);
                        formData.append('email_user', email_user);
                        formData.append('telephone_user', telephone_user);                        

                        // Set up the AJAX request.
                        var xhr = new XMLHttpRequest();

                        // Open the connection.
                        xhr.open('POST', 'include/insertUser.php', true);

                        // Set up a handler for when the request finishes.
                        xhr.onload = function () {
                          if (xhr.status === 200) {                  

                            //alert('Gracias. Hemos guardado tu información.');

                            $( '#name-error' ).hide();
                            $( '#email-error' ).hide();
                            $( '#email-error-exist' ).hide();                            

                            $("#name").val("");
                            $("#email").val("");
                            $("#telephone").val("");

                            location.href = "quest.php";
                            
                          } else {
                            
                            alert('Ha ocurrido un error. Por favor vuelve a intentarlo.');

                          }
                        };

                        // Send the Data.
                        xhr.send(formData);

                    }
                }
            }

            httpm.send( paramsm );
    }

}

$(document).ready(function() {    

    $('#checkbox_tc').change(function() {
        if(this.checked) {
            document.getElementById( 'div_submit_button' ).style.pointerEvents = 'auto';
            document.getElementById( 'div_submit_button' ).style.opacity = '';           
        } else {
            document.getElementById( 'div_submit_button' ).style.pointerEvents = 'none';
            document.getElementById( 'div_submit_button' ).style.opacity = '.3';
        }
        
    });


    /* Questions */
    /* 1q */
    /* 1o */
    $('#1-1').change(function() {
        if(this.checked) {
            
            $('#1-2').prop('checked', false);
            
            $('#1-3').prop('checked', false);
            
            $('#1-4').prop('checked', false);            

        } else {
            
            $('#1-2').prop('checked', false);
            
            $('#1-3').prop('checked', false);
            
            $('#1-4').prop('checked', false);
            
        }
        
    });
    /* End 1o */
    /* 2o */
    $('#1-2').change(function() {
        if(this.checked) {
            
            $('#1-1').prop('checked', false);
            
            $('#1-3').prop('checked', false);
            
            $('#1-4').prop('checked', false);
            

        } else {
            
            $('#1-1').prop('checked', false);
            
            $('#1-3').prop('checked', false);
            
            $('#1-4').prop('checked', false);
            
        }
        
    });
    /* End 2o */
    /* 3o */
    $('#1-3').change(function() {
        if(this.checked) {
            
            $('#1-1').prop('checked', false);
            
            $('#1-2').prop('checked', false);
            
            $('#1-4').prop('checked', false);
            

        } else {
            
            $('#1-1').prop('checked', false);
            
            $('#1-2').prop('checked', false);
            
            $('#1-4').prop('checked', false);
            
        }
        
    });
    /* End 3o */
    /* 4o */
    $('#1-4').change(function() {
        if(this.checked) {
            
            $('#1-1').prop('checked', false);
            
            $('#1-2').prop('checked', false);
            
            $('#1-3').prop('checked', false);
            

        } else {
            
            $('#1-1').prop('checked', false);
            
            $('#1-2').prop('checked', false);
            
            $('#1-3').prop('checked', false);
            
        }
        
    });
    /* End 4o */
    /* End 1q */

    /* 2q */
    /* 1o */
    $('#2-1').change(function() {
        if(this.checked) {
            
            $('#2-2').prop('checked', false);
            
            $('#2-3').prop('checked', false);
            
            $('#2-4').prop('checked', false);
            

        } else {
            
            $('#2-2').prop('checked', false);
            
            $('#2-3').prop('checked', false);
            
            $('#2-4').prop('checked', false);
            
        }
    });
    /* End 1o */
    /* 2o */
    $('#2-2').change(function() {
        if(this.checked) {
            
            $('#2-1').prop('checked', false);
            
            $('#2-3').prop('checked', false);
            
            $('#2-4').prop('checked', false);
            

        } else {
            
            $('#2-1').prop('checked', false);
            
            $('#2-3').prop('checked', false);
            
            $('#2-4').prop('checked', false);
            
        }
    });
    /* End 2o */
    /* 3o */
    $('#2-3').change(function() {
        if(this.checked) {
            
            $('#2-1').prop('checked', false);
            
            $('#2-2').prop('checked', false);
            
            $('#2-4').prop('checked', false);
            

        } else {
            
            $('#2-1').prop('checked', false);
            
            $('#2-2').prop('checked', false);
            
            $('#2-4').prop('checked', false);
            
        }
    });
    /* End 3o */
    /* 4o */
    $('#2-4').change(function() {
        if(this.checked) {
            
            $('#2-1').prop('checked', false);
            
            $('#2-2').prop('checked', false);
            
            $('#2-3').prop('checked', false);
            

        } else {
            
            $('#2-1').prop('checked', false);
            
            $('#2-2').prop('checked', false);
            
            $('#2-3').prop('checked', false);            

        }
    });
    /* End 4o */
    /* End 2q */

    /* 3q */
    /* 1o */
    $('#3-1').change(function() {
        if(this.checked) {
            
            $('#3-2').prop('checked', false);
            
            $('#3-3').prop('checked', false);
            
            $('#3-4').prop('checked', false);            

        } else {
            
            $('#3-2').prop('checked', false);
            
            $('#3-3').prop('checked', false);
            
            $('#3-4').prop('checked', false);            

        }
    });
    /* End 1o */
    /* 2o */
    $('#3-2').change(function() {
        if(this.checked) {
            
            $('#3-1').prop('checked', false);
            
            $('#3-3').prop('checked', false);
            
            $('#3-4').prop('checked', false);            

        } else {
            
            $('#3-1').prop('checked', false);
            
            $('#3-3').prop('checked', false);
            
            $('#3-4').prop('checked', false);
            
        }
    });
    /* End 2o */
    /* 3o */
    $('#3-3').change(function() {
        if(this.checked) {
            
            $('#3-1').prop('checked', false);
            
            $('#3-2').prop('checked', false);
            
            $('#3-4').prop('checked', false);            

        } else {
            
            $('#3-1').prop('checked', false);
            
            $('#3-2').prop('checked', false);
            
            $('#3-4').prop('checked', false);            

        }
    });
    /* End 3o */
    /* 4o */
    $('#3-4').change(function() {
        if(this.checked) {
            
            $('#3-1').prop('checked', false);
            
            $('#3-2').prop('checked', false);
            
            $('#3-3').prop('checked', false);            

        } else {
            
            $('#3-1').prop('checked', false);
            
            $('#3-2').prop('checked', false);
            
            $('#3-3').prop('checked', false);            

        }
    });
    /* End 4o */
    /* End 3q */  

    /* 4q */
    /* 1o */
    $('#4-1').change(function() {
        if(this.checked) {
            
            $('#4-2').prop('checked', false);
            
            $('#4-3').prop('checked', false);
            
            $('#4-4').prop('checked', false);
            
            $('#4-5').prop('checked', false);            

        } else {
            
            $('#4-2').prop('checked', false);
            
            $('#4-3').prop('checked', false);
            
            $('#4-4').prop('checked', false);
            
            $('#4-5').prop('checked', false);            

        }
    });
    /* End 1o */
    /* 2o */
    $('#4-2').change(function() {
        if(this.checked) {
            
            $('#4-1').prop('checked', false);
            
            $('#4-3').prop('checked', false);
            
            $('#4-4').prop('checked', false);
            
            $('#4-5').prop('checked', false);            

        } else {
            
            $('#4-1').prop('checked', false);
            
            $('#4-3').prop('checked', false);
            
            $('#4-4').prop('checked', false);
            
            $('#4-5').prop('checked', false);            

        }
    });
    /* End 2o */
    /* 3o */
    $('#4-3').change(function() {
        if(this.checked) {
            
            $('#4-1').prop('checked', false);
            
            $('#4-2').prop('checked', false);
            
            $('#4-4').prop('checked', false);
            
            $('#4-5').prop('checked', false);            

        } else {
            
            $('#4-1').prop('checked', false);
            
            $('#4-2').prop('checked', false);
            
            $('#4-4').prop('checked', false);
            
            $('#4-5').prop('checked', false);            

        }
    });
    /* End 3o */
    /* 4o */
    $('#4-4').change(function() {
        if(this.checked) {
            
            $('#4-1').prop('checked', false);
            
            $('#4-2').prop('checked', false);
            
            $('#4-3').prop('checked', false);
            
            $('#4-5').prop('checked', false);            

        } else {
            
            $('#4-1').prop('checked', false);
            
            $('#4-2').prop('checked', false);
            
            $('#4-3').prop('checked', false);
            
            $('#4-5').prop('checked', false);
        }
    });
    /* End 4o */
    /* 5o */
    $('#4-5').change(function() {
        if(this.checked) {
            
            $('#4-1').prop('checked', false);
            
            $('#4-2').prop('checked', false);
            
            $('#4-3').prop('checked', false);
            
            $('#4-4').prop('checked', false);            

        } else {
            
            $('#4-1').prop('checked', false);
            
            $('#4-2').prop('checked', false);
            
            $('#4-3').prop('checked', false);
            
            $('#4-4').prop('checked', false);            
        }
    });
    /* End 5o */
    /* End 4o */

    /* 5q */
    /* 1o */
    $('#5-1').change(function() {
        if(this.checked) {
            
            $('#5-2').prop('checked', false);
            
            $('#5-3').prop('checked', false);
            
            $('#5-4').prop('checked', false);            

        } else {
            
            $('#5-2').prop('checked', false);
            
            $('#5-3').prop('checked', false);
            
            $('#5-4').prop('checked', false);            
        }
    });
    /* End 1o */
    /* 2o */
    $('#5-2').change(function() {
        if(this.checked) {
            
            $('#5-1').prop('checked', false);
            
            $('#5-3').prop('checked', false);
            
            $('#5-4').prop('checked', false);            

        } else {
            
            $('#5-1').prop('checked', false);
            
            $('#5-3').prop('checked', false);
            
            $('#5-4').prop('checked', false);            
        }
    });
    /* End 2o */
    /* 3o */
    $('#5-3').change(function() {
        if(this.checked) {
            
            $('#5-1').prop('checked', false);
            
            $('#5-2').prop('checked', false);
            
            $('#5-4').prop('checked', false);            

        } else {
            
            $('#5-1').prop('checked', false);
            
            $('#5-2').prop('checked', false);
            
            $('#5-4').prop('checked', false);            

        }
    });
    /* End 3o */
    /* 4o */
    $('#5-4').change(function() {
        if(this.checked) {
            
            $('#5-1').prop('checked', false);
            
            $('#5-2').prop('checked', false);
            
            $('#5-3').prop('checked', false);            

        } else {
            
            $('#5-1').prop('checked', false);
            
            $('#5-2').prop('checked', false);
            
            $('#5-3').prop('checked', false);            
        }
    });
    /* End 4o */
    /* End 5q */

    /* 6q */
    /* 1o */
    $('#6-1').change(function() {
        if(this.checked) {
            
            $('#6-2').prop('checked', false);
            
            $('#6-3').prop('checked', false);
            
            $('#6-4').prop('checked', false);            

        } else {
            
            $('#6-2').prop('checked', false);
            
            $('#6-3').prop('checked', false);
            
            $('#6-4').prop('checked', false);            
        }
    });
    /* End 1o */
    /* 2o */
    $('#6-2').change(function() {
        if(this.checked) {
            
            $('#6-1').prop('checked', false);
            
            $('#6-3').prop('checked', false);
            
            $('#6-4').prop('checked', false);            

        } else {
            
            $('#6-1').prop('checked', false);
            
            $('#6-3').prop('checked', false);
            
            $('#6-4').prop('checked', false);            
        }
    });
    /* End 2o */
    /* 3o */
    $('#6-3').change(function() {
        if(this.checked) {
            
            $('#6-1').prop('checked', false);
            
            $('#6-2').prop('checked', false);
            
            $('#6-4').prop('checked', false);            

        } else {
            
            $('#6-1').prop('checked', false);
            
            $('#6-2').prop('checked', false);
            
            $('#6-4').prop('checked', false);            
        }
    });
    /* End 3o */
    /* 4o */
    $('#6-4').change(function() {
        if(this.checked) {
            
            $('#6-1').prop('checked', false);
            
            $('#6-2').prop('checked', false);
            
            $('#6-3').prop('checked', false);            

        } else {
            
            $('#6-1').prop('checked', false);
            
            $('#6-2').prop('checked', false);
            
            $('#6-3').prop('checked', false);
        
        }
    });
    /* End 4o */
    /* End 6q */

});

function doQuestionsSubmit() {

    var values1 = new Array();
    $.each($("input[name='optionsCheckboxes1']:checked"), function() {
      values1.push($(this).val());
    });

    var values2 = new Array();
    $.each($("input[name='optionsCheckboxes2']:checked"), function() {
      values2.push($(this).val());
    });

    var values3 = new Array();
    $.each($("input[name='optionsCheckboxes3']:checked"), function() {
      values3.push($(this).val());
    });

    var values4 = new Array();
    $.each($("input[name='optionsCheckboxes4']:checked"), function() {
      values4.push($(this).val());
    });

    var values5 = new Array();
    $.each($("input[name='optionsCheckboxes5']:checked"), function() {
      values5.push($(this).val());
    });

    var values6 = new Array();
    $.each($("input[name='optionsCheckboxes6']:checked"), function() {
      values6.push($(this).val());
    });

    suma = parseInt(values1[0])+parseInt(values2[0])+parseInt(values3[0])+parseInt(values4[0])+parseInt(values5[0])+parseInt(values6[0]);
    var user_id = document.getElementById("user_id").value;

    location.href = "confirmation.php?u_id="+user_id+"&r_s="+suma;
}

function doEvaluation() {

    var chks1 = document.getElementsByName('optionsCheckboxes1');


    var hasChecked1 = false;

    for (var a=0;a<chks1.length;a++) {

        if(chks1[a].checked){
            hasChecked1 = true;
            break;
        }

    }

    var chks2 = document.getElementsByName('optionsCheckboxes2');

    var hasChecked2 = false;

    for (var b=0;b<chks2.length;b++) {

        if(chks2[b].checked){
            hasChecked2 = true;
            break;
        }

    }

    var chks3 = document.getElementsByName('optionsCheckboxes3');

    var hasChecked3 = false;

    for (var c=0;c<chks3.length;c++) {

        if(chks3[c].checked){
            hasChecked3 = true;
            break;
        }

    }

    var chks4 = document.getElementsByName('optionsCheckboxes4');

    var hasChecked4 = false;

    for (var d=0;d<chks4.length;d++) {

        if(chks4[d].checked){
            hasChecked4 = true;
            break;
        }

    }

    var chks5 = document.getElementsByName('optionsCheckboxes5');

    var hasChecked5 = false;

    for (var e=0;e<chks5.length;e++) {

        if(chks5[e].checked){
            hasChecked5 = true;
            break;
        }

    }

    var chks6 = document.getElementsByName('optionsCheckboxes6');

    var hasChecked6 = false;

    for (var f=0;f<chks6.length;f++) {

        if(chks6[f].checked){
            hasChecked6 = true;
            break;
        }

    }

    if (hasChecked1 &&
        hasChecked2 &&
        hasChecked3 &&
        hasChecked4 &&
        hasChecked5 &&
        hasChecked6) {

        document.getElementById( 'div_submit_questions' ).style.pointerEvents = 'auto';
        document.getElementById( 'div_submit_questions' ).style.opacity = '';

    } else {

        document.getElementById( 'div_submit_questions' ).style.pointerEvents = 'none';
        document.getElementById( 'div_submit_questions' ).style.opacity = '.3';
    }

}