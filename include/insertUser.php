<?php
require_once('DB_Functions.php');
$db = new DB_Functions();

if (isset($_POST['email_user'])):
    // receiving the post params    
    $name_user  = utf8_decode($_POST['name_user']);
    $email_user = $_POST['email_user'];
    $telephone_user = $_POST['telephone_user'];
    
    $insertUser = $db->insertUser($name_user, $email_user, $telephone_user);

    if ($insertUser != false):
        
        echo "Gracias. Hemos guardado tu información.";

    else:

        echo "Ha ocurrido un error. Vuelve a intentarlo.";

    endif;
    
else:

    echo "Ha ocurrido un error. Vuelve a intentarlo.";

endif;
?>