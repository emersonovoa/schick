<?php
require_once('DB_Functions.php');
$db = new DB_Functions();

if (isset($_POST['id_user'])):
    // receiving the post params
    $id_user       = $_POST['id_user'];
    $ids_questions = $_POST['ids_questions'];
    $ids_answers   = $_POST['ids_answers'];
    $profile_user  = $_POST['profile_user'];

    $questions = count($ids_questions);

    $flag_over = False;

    for ($x = 0; $x <= $questions; $x++) {
    
        $id_question = $ids_questions[$x];
        $id_answer   = $ids_answers[$x];

        $insertUserQA = $db->insertUserQA($id_user, $id_question, $id_answer);

        if ($x == $questions):

            $flag_over = True;

        endif;

    }

    if ($flag_over):

        $insertUserProfile = $db->insertUserProfile($id_user, $profile_user);

        if ($insertUserProfile != false):

            echo "Gracias. Hemos guardado tu información.";            

        else:

            echo "Ha ocurrido un error. Vuelve a intentarlo.";            

        endif;

        echo "Gracias. Hemos guardado tu información.";

    else:
      
        echo "Ha ocurrido un error. Vuelve a intentarlo.";

    endif;
    
else:

    echo "Ha ocurrido un error. Vuelve a intentarlo.";

endif;
?>