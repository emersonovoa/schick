<?php
@ob_start();
session_start();

header('Content-Type: text/html; charset=utf-8');
/**
 * @author Emerson Novoa
 * @from MULLENLOWE MX
*/
 
class DB_Functions {
 
    private $conn;
 
    // constructor
    function __construct() {
        require_once("DB_Connect.php");
        // connecting to database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }
 
    // destructor
    function __destruct() {}    

    /**
     * Check user if exist or not
     */
    public function userExist($email) {

      $stmt = $this->conn->prepare("SELECT Email from users WHERE Email = ? AND Active = 1;");
      $stmt->bind_param("s", $email);
      $stmt->execute();
      $stmt->store_result();

      if ($stmt->num_rows > 0):
          // user exist
          $stmt->close();
          return true;
      else:
          // user not exist
          $stmt->close();
          return false;
      endif;

    }

    /**
     * Storing users     
     */    
    public function insertUser($name_user, $email_user, $phone_user) {

      $query_insert = "INSERT INTO users(Name,
                                         Email,
                                         Telephone,
                                         Photo,
                                         Active,
                                         Date_Mod)
                       VALUES('$name_user',
                              '$email_user',
                              '$phone_user',
                              '',
                              1,
                              NOW());";
      
      if($stmt_insert = $this->conn->prepare($query_insert)):
        $stmt_insert->execute();
        $stmt_insert->free_result();
        $stmt_insert->close();

        /* Getting record by email address to get id_user and put in session*/
        $query_user = "SELECT * FROM users WHERE Email = '$email_user';";
        $stmt_user = $this->conn->prepare($query_user);
        if ($stmt_user->execute()) {
            $user = $stmt_user->get_result()->fetch_assoc();
            $stmt_user->close();
            $user_id    = $user['ID_User'];
            $user_name  = $user['Name'];
            $user_mail  = $user['Email'];

            $_SESSION["user_id"] = $user_id;
            $_SESSION["name"]    = $user_name;
            $_SESSION["email"]   = $user_mail;
            
        }

        return true;
      else:
        return NULL;
      endif;

    }

    /**
     * Storing users questions answers     
     */    
    public function insertUserQA($id_user, $id_question, $id_answer) {

      $query_insert = "INSERT INTO user_question_answer(ID_User,
                                         ID_Question,
                                         Answer,
                                         Date_Mod)
                       VALUES($id_user,
                              $id_question,
                              $id_answer,
                              NOW());";
      
      if($stmt_insert = $this->conn->prepare($query_insert)):
        $stmt_insert->execute();
        $stmt_insert->free_result();
        $stmt_insert->close();
        return true;
      else:
        return NULL;
      endif;

    }

    /**
     * Storing users profiles     
     */    
    public function insertUserProfile($id_user, $profile_user) {

      $query_insert = "INSERT INTO user_profile(ID_User,
                                         ID_Profile,                                         
                                         Date_Mod)
                       VALUES($id_user,
                              $profile_user,                              
                              NOW());";
      
      if($stmt_insert = $this->conn->prepare($query_insert)):
        $stmt_insert->execute();
        $stmt_insert->free_result();
        $stmt_insert->close();
        return true;
      else:
        return NULL;
      endif;

    }
}
?>