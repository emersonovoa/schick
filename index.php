<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="">
	<link rel="icon" type="image/png" href="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Schick Hydro Sensitive</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/material-kit.css" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet"/>
</head>

<body >
<div class="back2"></div>	
<div id="pleca"></div>

<div class="section-full-screen  bg-overlay >
        <div class="container col-md-8 col-md-offset-2">
            <div class="row">
                
                	<div class="content-center ">
                    <img class="n-logo-home" src="./assets/img/Logo-shick.png" alt="Logo Schick Hydro">
                    <h1 class="h1-seo">Bienvenido a la Experiencia Schick Hydro</h1>
                    <h3 class="h3-seo">Con este test podrás conocer el tipo de barba que va mejor con tu estilo y forma de vida. </h3>
                    <h3 id="h3-copy">Así que, adelante y disfruta conociendo el mejor estilo para ti.</h3>
                    </br>
                    <div class="btn-space text-center">
					    <a href="registry.php" class="btn btn-success btn-lg">Comenzar</a>
					</div>
                
            </div>
		</div>
 </div>


 




</body>

	<!--   Core JS Files   -->
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/material.min.js"></script>

	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="assets/js/nouislider.min.js" type="text/javascript"></script>

	<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
	<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

	<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
	<script src="assets/js/material-kit.js" type="text/javascript"></script>

</html>
