<?php
/* FILAS ALTERNAS */
$row_highlite = 0;

require_once('../include/Config_GET.php');

$query = 'SELECT u.ID_User, u.Name, u.Email, u.Telephone, up.ID_Profile, up.Points
		  FROM users u
		  LEFT JOIN user_profile up
		  ON u.ID_User = up.ID_User
		  WHERE u.Active = 1';

$result = $conn->query($query);

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	document.addEventListener('contextmenu', event => event.preventDefault());

    $("#btnExport").click(function(e) {
      e.preventDefault();

      //getting data from our table
      //var data_type = 'data:application/vnd.ms-excel';
      var data_type = 'data:application/vnd.ms-excel;based64';
      var table_div = document.getElementById('table-data');
      var table_html = table_div.outerHTML.replace(/ /g, '%20');

      var a = document.createElement('a');
      a.href = data_type + ', ' + table_html;
      //a.download = 'exported_table_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
      a.download = 'registros_schick.xls';
      a.click();
    });

});
</script>
<style type="text/css">
	/* Add a hover effect (blue shadow) */
body {
  font-size: 11px !important;
  /*background-color:#000!important;*/
  color: #FFF;
  background: linear-gradient(rgba(11,60,103,.9), rgba(16,23,41,100)70% ), url('../img/schick3.gif') center center no-repeat scroll;
}
img {
    border: none; /* Gray border */
    border-radius: 4px;  /* Rounded border */
    padding: 5px; /* Some padding */
    width: 250px; /* Set a small width */
    outline: none;
}

img:hover {
    /*box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);*/
}

table, th, td {
  text-align: left;
  padding: 1px 5PX;
}
table{
  border: 1px solid #C5C5C5;
  }
th{
  background-color: #79E400;
  color: #FFF;
  font-size: 14px;
  padding: 5px 10px;
}
.row_highlite{
  background-color: #79E400;
}
  a{
    color: #FFF;
  }
</style>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
			
<?php						

if ($result->num_rows > 0) {
  // output data of each row
?>

<button id="btnExport">Exportar a excel</button>
<div style="height: 20px;">&nbsp;</div>

<div id="table-data">
	<table cellspacing="0" id="list" cellpadding="0" border="0">
		<tr>
		    <th>
		      <div style="height: 50px;width: 100%"></div>
		      ID
		    </th>
		    <th>
		      <div style="height: 50px;width: 100%"></div>
		      NOMBRE
		    </th>
		    <th>
		      <div style="height: 50px;width: 100%"></div>
		      EMAIL
		    </th>
		    <th>
		      <div style="height: 50px;width: 100%"></div>
		      TEL&Eacute;FONO
		    </th>    
		    <th>
		      <div style="height: 50px;width: 100%"></div>
		      PERFIL
		    </th>
		    <th>
		      <div style="height: 50px;width: 100%"></div>
		      PUNTUACI&Oacute;N
		    </th>    
	  	</tr>

<?php

  $count_id = 0;
  while($row = $result->fetch_assoc()) {

  	$count_id = $count_id + 1;

?>

    	<tr <?php echo $row_highlite++ % 2 ? "class=row_highlite" : ""; ?>>        
		    <td><?=$row["ID_User"];?></td>		    
		    <td><?=utf8_encode($row["Name"]);?></td>
		    <td><a href="mailto:<?php echo $row["Email"]; ?>" target="_blank"><?=$row["Email"];?></a></td>
		    <td><?=utf8_encode($row["Telephone"]);?></td>
		    <td>
		    	<div style="position: relative; overflow: hidden; width: 250px; height: 250px;">

		    		<?php
		    		if ($row["ID_Profile"] == 1):
		    		?>
        				<img src="../assets/img/Comp-1.gif" src="Ras">
        			<?php
		    		elseif ($row["ID_Profile"] == 2):
		    		?>
		    			<img src="../assets/img/Comp-2.gif" src="Natural">
		    		<?php
		    		elseif ($row["ID_Profile"] == 3):
		    		?>
		    			<img src="../assets/img/Comp-3.gif" src="Ejecutivo">
		    		<?php
		    		else:
		    		?>
		    			<img src="../assets/img/Comp-4.gif" src="Full Beard">
		    		<?php
		    		endif;
		    		?>
		    	</div>
		    </td>
		    <td><?=utf8_encode($row["Points"]);?></td>
		</tr>

    <?php

    }

}
?>    

  </table>
</div>